// 当前第几页
let slideIndex = 1;
// $("#playerList").css({"left": "-1800px"});
let mover = function (animateLength) {
    $("#playerList").animate({"left": animateLength}, "slow")
    if (slideIndex===1) {
        $("#page-01-box").css({"background-color": "rgba(0,0,0,.3)"});
        $("#page-02-box").css({"background-color": "rgba(0,0,0,.1)"});
    }
    if (slideIndex===2) {
        $("#page-01-box").css({"background-color": "rgba(0,0,0,.1)"});
        $("#page-02-box").css({"background-color": "rgba(0,0,0,.3)"});
    }
}

//左边按钮的点击事件
$("#playerLeftBtn").on("click", function () {
    // 第一页++，第二页--
    slideIndex === 1 ? slideIndex++ : slideIndex--;
    mover((slideIndex - 1) * (-1200) + "px")
})
//右边按钮的点击事件
$("#playerRightBtn").on("click", function () {
    // 第二页--，第一++
    slideIndex === 2 ? slideIndex-- : slideIndex++;
    mover((slideIndex - 1) * (-1200) + "px")
})

$("#page-01-box").on("click", function () {
    if (slideIndex === 1) {
        return;
    } else {
        slideIndex--;
        mover((slideIndex - 1) * (-1200) + "px")
    }
})

$("#page-02-box").on("click", function () {
    if (slideIndex === 2) {
        return;
    } else {
        slideIndex++;
        mover((slideIndex - 1) * (-1200) + "px")
    }
})